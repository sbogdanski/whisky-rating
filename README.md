# Rate Your Whisky!
This project is a simple example to try and figure out how React + MobX work. It is based on the classical example
for a ToDo-App on ReactJS, but then tweaked. The changes somehow grew more and more over time.

## What it uses
It is one page of React-components, and one MobX store that holds the data.
I'm using Material-UI to provide user input - can start learning that as well,
while I'm on it.

## Data Storage
Used to be localStorage in the users browser, is now stored in a json file on the server via php scripts.

### Future Storage
I'm thinking about migration from server file storage (not safe, especially when multiple users use it) to Firebase. Will see how that goes at a later date in the future.
