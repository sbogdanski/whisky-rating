/* this is the model for my whisky.
 * it creates an uuid for every whisky; every new instances gets assigned a new
 * v4 uuid, thanks to the node.js package that delivers this function - very nice!
 * So, the whisky has an internal ID (not visible to the user), it has a Distillery,
 * a Description (which will either be an age or a name), 
 * and a rating ranging from 0 to 5 stars; 0 means "not yet rated"
 */
 import {observable, computed} from 'mobx';
 import {v4} from 'uuid';

 export class Whisky {
   /* assign new UUID to every new instance */
   id = v4();

   @observable distillery = '';
   @observable description = '';
   @observable notes = '';
   @observable tastedBy = '';
   @observable tastedOn = null;
   @observable rating = 0;
   @observable pricePerBottle = 0;

   /* a valid whisky needs distillery, description and a rating in the valid range */
   @computed get isValid() { 
     return this.description !== '' && this.distillery !== '' && this.tastedBy !== '';
   }

   /* the next two methods should be replaced in a later version by something more
    * fit to the task, like github.com/mobxjs/serializr
    */
   serialize() {
     return {
       id: this.id,
       distillery: this.distillery,
       description: this.description,
       rating: this.rating,
       notes: this.notes,
       tastedBy: this.tastedBy,
       tastedOn: this.tastedOn,
       pricePerBottle: this.pricePerBottle
     };
   }

   static deserialize(json) {
     const whisky = new Whisky();
     whisky.id = json['id'] || v4();
     whisky.distillery = json['distillery'] || '';
     whisky.description = json['description'] || '';
     whisky.rating = parseInt(json['rating'], 10) || 0;
     whisky.notes = json['notes'] || '';
     whisky.tastedBy = json['tastedBy'] || '';
     whisky.tastedOn = new Date(json['tastedOn']) || null;
     whisky.pricePerBottle = parseInt(json['pricePerBottle'], 10) || 0;
     return whisky;
   }
 }