/* This is the View for the Whisky Data Mode
 * It uses Material-UI, which I'll have to figure out along the way
 */
import React from 'react';
import {observer} from 'mobx-react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Snackbar from 'material-ui/Snackbar';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import Slider from 'material-ui/Slider';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import ContentAddCircleOutline from 'material-ui/svg-icons/content/add';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';
import ContentSave from 'material-ui/svg-icons/content/save';
import ContentUnarchive from 'material-ui/svg-icons/content/unarchive';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import {red500, orange500, blue800, green800, redA100, grey500, blue500} from 'material-ui/styles/colors';

const styles = {
  underlineStyle: {
    borderColor: orange500,
  },
  buttonStyle: {
    marginRight: "30px",
  },
  arrowStyle: {
    marginRight: "5px",
  },
  boldStyle: {
    fontWeight: "bold",
    textColor: blue800,
    height: '40px',
    marginBottom: '5px',
    marginTop: '5px',
  },
  textStyle: {
    height: '40px',
    marginBottom: '5px',
    marginTop: '5px',
    autoWidth: 'true',
  },
  snackStyle: {
    top: 0,
    bottom: 'auto',
    left: (window.innerWidth - 288) / 2,
    transform: 'translate3d(0, -50px, 0)',
  },
  inputStyle: {
    fontSize: "10pt",
  },
  blue: {
    color: blue800,
  },
  green: {
    color: green800,
  },
  orange: {
    color: orange500,
  },
};

/* React Component.
 * The property model of the passed props object is an instance of the
 * WhiskyViewModel class
 */
@observer
export class WhiskyView extends React.Component {
  render() {
    const model = this.props.model;
    return (
      <MuiThemeProvider>
        <div>
          <h1>Rate your Whiskies!</h1>
          <div>
            <FloatingActionButton style={styles.buttonStyle} onClick={() => model.add()} label="New">
              <ContentAddCircleOutline />
            </FloatingActionButton>
            <FloatingActionButton style={styles.buttonStyle} onClick={() => model.save()} 
              secondary={true} label="Save">
              <ContentSave />
            </FloatingActionButton>
            <FloatingActionButton style={styles.buttonStyle} onClick={() => model.load()} 
              secondary={true} label="Load">
              <ContentUnarchive />
            </FloatingActionButton>
          </div>
        </div>
        <Table selectable={false} >
          <TableHeader displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn colSpan='4'>
                Distillery and Name
                <NavigationArrowDropUp style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByName()}/>
                <NavigationArrowDropDown style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByName(true)}/>
                </TableHeaderColumn>
              <TableHeaderColumn colSpan='6'>Notes</TableHeaderColumn>
              <TableHeaderColumn colspan='3'>
                Tasted by and on
                <NavigationArrowDropUp style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByDate()}/>
                <NavigationArrowDropDown style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByDate(true)}/>
              </TableHeaderColumn>
              <TableHeaderColumn colspan='3'>
                Price per Bottle
                <NavigationArrowDropUp style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByPrice()}/>
                <NavigationArrowDropDown style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByPrice(true)}/>
              </TableHeaderColumn>
              <TableHeaderColumn colspan='3'>
                Rating
                <NavigationArrowDropUp style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByRating()}/>
                <NavigationArrowDropDown style={styles.arrowStyle} color={blue500} hoverColor={orange500} onClick={() => model.sortByRating(true)}/>
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody stripedRows={true} d>
            {model.whiskies.map((whisky, i) => <SingleWhiskyView key={whisky.id} model={model} whisky={whisky} />)}
          </TableBody>
        </Table>
        <Snackbar
          open={model.is_saved}
          message="Data written to server"
          autoHideDuration={4000}
          onRequestClose={() => model.is_saved = false}
        />
        <Snackbar
          open={model.is_loaded}
          message="Data loaded from server"
          autoHideDuration={4000}
          onRequestClose={() => model.is_loaded = false}
        />
        <Snackbar
          open={model.is_sorted}
          message="Data has been sorted"
          autoHideDuration={4000}
          onRequestClose={() => model.is_loaded = false}
        />
      </MuiThemeProvider>
    );
  }
}

/* this is the view for a single whisky */
@observer
export class SingleWhiskyView extends React.Component {
  render() {
    /* ...otherProps are passed down from the TableBody in the other component */
    const { model, whisky, ...otherProps} = this.props;
    var ratingColor = '';
    switch (whisky.rating) {
      case 1: case 2: ratingColor = {fontWeigth: 'bold', color: redA100}; break;
      case 3: ratingColor = {fontWeigth: 'bold', color: blue800}; break;
      case 4: case 5: ratingColor = {fontWeigth: 'bold', color: green800}; break;
      default: ratingColor = {fontWeigth: 'bold', color: grey500}; break;
    };
    return (
      <TableRow displayBorder={false} {...otherProps}>
        <TableRowColumn colSpan="2">
          <TextField name="distillery" type="text" underlineFocusStyle={styles.underlineStyle} style={styles.boldStyle}
            hintText="Name of the Distillery" value={whisky.distillery} onChange={e => { whisky.distillery = e.target.value }} />
        </TableRowColumn>
        <TableRowColumn colSpan="2">
          <TextField name="description" type="text" underlineFocusStyle={styles.underlineStyle} style={styles.textStyle}
            hintText="Name of the Whisky (or age)" value={whisky.description} onChange={e => { whisky.description = e.target.value }} />
        </TableRowColumn>
        <TableRowColumn colSpan="6" >
          <TextField name="notes" type="text" underlineFocusStyle={styles.underlineStyle} multiLine={true}
            hintText="Notes about the tasting" rows='2' fullWidth={true} inputStyle={styles.inputStyle} 
            value={whisky.notes} onChange={e => { whisky.notes = e.target.value }} />
        </TableRowColumn>
        <TableRowColumn colSpan="2" >
          <TextField name="tastedBy" type="text" underlineFocusStyle={styles.underlineStyle} style={styles.textStyle}
            hintText="Who did the tasting?" value={whisky.tastedBy} onChange={e => { whisky.tastedBy = e.target.value }} />
        </TableRowColumn>
        <TableRowColumn colSpan="2"> 
          <DatePicker mode="landscape"  hintText="When was the tasting?"  inputStyle={styles.inputStyle}  style={styles.textStyle}
            value={whisky.tastedOn} onChange={(e, date) => whisky.tastedOn = date} />
        </TableRowColumn>
        <TableRowColumn colSpan="2"> 
           <Slider min={0} max={150} step={5} value={whisky.pricePerBottle} onChange={(e, value) => {whisky.pricePerBottle = value}} />
        </TableRowColumn>
        <TableRowColumn >
          <p style={(whisky.pricePerBottle < 41 && styles.green) || (whisky.pricePerBottle < 81 && styles.blue) || styles.orange}>
            <span style={{fontWeight: "bold"}}>{whisky.pricePerBottle} €</span>
          </p>
        </TableRowColumn>
        <TableRowColumn colspan="2">
          <SelectField value={whisky.rating} labelStyle={ratingColor} style={styles.inputStyle} 
            onChange={(e, i, v) => {whisky.rating = parseInt(v, 10)}}>
            <MenuItem value={0} primaryText="Not yet rated" />
            <MenuItem value={1} primaryText="Miserable" />
            <MenuItem value={2} primaryText="Bad" />
            <MenuItem value={3} primaryText="Average" />
            <MenuItem value={4} primaryText="Good" />
            <MenuItem value={5} primaryText="Superb" />
          </SelectField>
        </TableRowColumn>
        <TableRowColumn >
          <ActionDelete style={styles.buttonStyle} hoverColor={red500} onClick={() => model.remove(whisky)}/>
        </TableRowColumn>
      </TableRow>
    );
  }
}