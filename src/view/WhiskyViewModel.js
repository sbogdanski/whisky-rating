/* this is the ViewModel that holds the Collection of Whiskies.
 * it uses observables and actions from mobx state manager.
 */
import {observable, action} from 'mobx';
import {Whisky} from '../model/Whisky';

const server_path = "http://www.zweiundvierzich.de/jsons/";
const file_name = "whisky_rating.json";

export class WhiskyViewModel {

  /* the observable list of all whiskies */
  @observable whiskies = [];
  @observable is_loaded = false;
  @observable is_saved = false;
  @observable is_sorted = false;

  /* try to load the list when model is constructed */
  constructor() {
    this.load();
  }

  /* actions to add or remove a whisky to the list */
  @action
  add() {
    const newWhisky = new Whisky();
    this.whiskies.unshift(newWhisky);
    return newWhisky;
  }

  @action
  remove(whisky) {
    const index = this.whiskies.indexOf(whisky);
    if (index > -1) {
      this.whiskies.splice(index, 1);
    }
  }
  
  /* loading and saving data to/from local Storage in the browser */
  @action
  load() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", server_path + "read-json.php?file=" + file_name, true);
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        const json = JSON.parse(xhr.responseText || "[]");
        this.whiskies = json.map(whisky => Whisky.deserialize(whisky));
        this.is_loaded = true;
      }
    };
    xhr.send();
    return this.whiskies;
  }

  @action
  save() {
    if (this.whiskies.filter(whisky => !whisky.isValid).length) {
      alert("Unable to save: There are invalid whisky ratings (not fully declared)");
    } else {
      var data = JSON.stringify(this.whiskies.map(whisky => Whisky.deserialize(whisky)));
      var request = new XMLHttpRequest();
      var URL = server_path + "save-json.php?file=" + file_name + "&data=" + encodeURI(data);
      request.open("GET", URL);
      request.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
      request.send();
      this.is_saved = true;
    }
  }

  @action
  sortByName(reversed = false) {
    /* sort by Name of Distillery, followed by Name of Whisky */
    if (this.whiskies.filter(whisky => !whisky.isValid).length) {
      alert("Unable to sort: There are invalid whisky ratings (not fully declared)");
    } else {
     this.whiskies = this.whiskies.sort((a, b) => {
        const distA = a.distillery.toLowerCase(), distB = b.distillery.toLowerCase();
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        const nameA = a.description.toLowerCase(), nameB = b.description.toLowerCase();
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
      });
      if (reversed) this.whiskies = this.whiskies.reverse();
      this.is_sorted = true;
    }
  }

  @action
  sortByDate(reversed = false) {
    /* sort by Date of Tasting, followed by Name of Distillery */
    if (this.whiskies.filter(whisky => !whisky.isValid).length) {
      alert("Unable to sort: There are invalid whisky ratings (not fully declared)");
    } else {
      this.whiskies = this.whiskies.sort((a, b) => {
        const timeDiff = a.tastedOn - b.tastedOn;
        if (timeDiff) return timeDiff;
        const distA = a.distillery.toLowerCase(), distB = b.distillery.toLowerCase();
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
      });
      if (reversed) this.whiskies = this.whiskies.reverse();
      this.is_sorted = true;
    }
  }

  @action
  sortByPrice(reversed = false) {
    /* sort by Rating, followed by Name of Distillery */
    if (this.whiskies.filter(whisky => !whisky.isValid).length) {
      alert("Unable to sort: There are invalid whisky ratings (not fully declared)");
    } else {
      this.whiskies = this.whiskies.sort((a, b) => {
        const pricePerBottleDiff = a.pricePerBottle - b.pricePerBottle;
        if (pricePerBottleDiff) return pricePerBottleDiff;
        const ratingDiff = a.rating - b.rating;
        if (ratingDiff) return ratingDiff;
        const distA = a.distillery.toLowerCase(), distB = b.distillery.toLowerCase();
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
      });
      if (reversed) this.whiskies = this.whiskies.reverse();
      this.is_sorted = true;
    }
  }

  @action
  sortByRating(reversed = false) {
    /* sort by Rating, followed by Name of Distillery */
    if (this.whiskies.filter(whisky => !whisky.isValid).length) {
      alert("Unable to sort: There are invalid whisky ratings (not fully declared)");
    } else {
      this.whiskies = this.whiskies.sort((a, b) => {
        const ratingDiff = a.rating - b.rating;
        if (ratingDiff) return ratingDiff;
        const distA = a.distillery.toLowerCase(), distB = b.distillery.toLowerCase();
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
      });
      if (reversed) this.whiskies = this.whiskies.reverse();
      this.is_sorted = true;
    }
  }

 }