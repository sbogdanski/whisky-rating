// import needed libraries
import React from 'react';
import {render} from 'react-dom';

// import view and viewModel
import {WhiskyView} from './view/WhiskyView';
import {WhiskyViewModel} from './view/WhiskyViewModel';

// tap events of mobile devices
import injectTapEventPlugin from 'react-tap-event-plugin';
// needed for onTouchTap by material-ui; http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

// create a viewModel singleton
const model = new WhiskyViewModel();

// render the editor, example
render(<WhiskyView model={model} />, document.getElementById('root'))
